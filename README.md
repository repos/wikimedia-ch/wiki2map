# Wiki2Map

Wiki2Map is an interactive mindmap generator for Wikitext-based resources, like a Wikipedia article.

https://wiki2map.org/

## History

With the version 2.0, the engine has been completely rewritten and the UI given a new, fresher look!
Locale support has been added as well, please contribute to making wiki2map accessible in as many languages as possible!

HTTP GET parameter support is now available to have wiki2map automatically load an article with its UI set up in a (supported) language of choice. For example https://wiki2map.org/index.html?wiki=en.wikipedia.org&topic=Earth&lang=en will load the english Wikipedia article about planet Earth with the UI in english.
New controls have been added and unused ones removed.

For developers info, see [the wiki](https://github.com/WikimediaSwitzerland/wiki2map/wiki).

TODO: This technical documentation could be migrated in a Markdown file, or to MediaWiki.org or something similar.

## Code Contributors

You can join Wikimedia GitLab and Fork:

https://gitlab.wikimedia.org/repos/wikimedia-ch/wiki2map

Thanks for your code contributions!

## Bug Reporting

You can report public Tasks and Ideas in Wikimedia Phabricator:

https://phabricator.wikimedia.org/tag/wiki2map.org/

Thanks for your time!

## Update Production

The website Wiki2Map.org is hosted in the WMCH Infrastructure in the server `wmch-mores-members2`.

Document root:

    /var/www/wiki2map.org/www

To update, just run a `git pull` inside.

The infrastructure documentation is available here:

https://members.wikimedia.ch/wiki/Infrastructure
